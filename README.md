# Tacotron2 Speech Synthesis

Training TTS model with Tacotron2 framework

# Requirements 

Install requirements:
```
pip install -r requirements.txt
```

# Data format

Data format: Mono wav, sampling rate: 22050, Decoding: PCMS16LE

Use ffmpeg for data format.

# Crawl data(option)

Put the video link in playlists.txt and run:

```
youtube_crawl.py
```

# Preprocess and make db for training tacotron2
If you crawl data from youtube. Use autosub API by:
```
python transcript.py
python make_db.py
python split.py
```

If you use data from another source, just format data like training.txt and testing samples.

# Training
Change link to the files: training.txt and testing.txt in tacotron2/hparams.py

Training model:
```
python train.py -o output_vi -l logs
```

# Testing
Test model with tacotron2/inference.ipynb
