import pysrt
from pydub import AudioSegment
import os
import pandas as pd
from normalize import normalize_text

# Define lambda function convert time to miliseconds 
time_to_ms = lambda x: (x.hours*3600 + x.minutes *60 + x.seconds) * 1000 + x.milliseconds

# Preprare data function
def prepare_data(raw_audio_dir, audio_outdir, csv_output):
    data_df = pd.DataFrame(columns=['audio', 'sub'])
    audios_list, subs_list = [], []
    sub_file = os.listdir(raw_audio_dir)
    # Interative over files
    for f in sub_file:
        if f.endswith(".wav"):
            # Audio and sub dir
            audio_path = os.path.join(raw_audio_dir, f)
            audio_name, audio_format = f.split('.')
            audio_sub_name = audio_name+'.srt'
            audio_sub_path = os.path.join(raw_audio_dir, audio_sub_name)
            #Read data
            audio = AudioSegment.from_file(audio_path)
            subs = pysrt.open(audio_sub_path, encoding = 'utf-8')
            for sub in subs:
                # Extract time
                start_ms = time_to_ms(sub.start)
                end_ms = time_to_ms(sub.end)
                audio_extract_name = f'{audio_outdir}/{audio_name}_{start_ms}_{end_ms}.wav'
                text = normalize_text(str(sub.text))
                # CSV Columns
                audios_list.append(audio_extract_name)
                subs_list.append(text)
                # Extract to file
                extract = audio[start_ms:end_ms]
                extract.export(audio_extract_name, format="wav")
    # To csv file
    data_df['audio'], data_df['sub'] = audios_list, subs_list
    data_df.to_csv(csv_output, index=False)


if __name__ == "__main__":
    audio_outdir = 'audios'
    csv_output = 'data.csv'
    raw_audio_dir = 'audio'
    
    if not os.path.exists(audio_outdir):
        os.makedirs(audio_outdir)
    
    prepare_data(raw_audio_dir, audio_outdir, csv_output)

