# Training testing split
import pandas as pd
import os
from sklearn.model_selection import train_test_split


#Read data
data = pd.read_csv('data.csv')
audio, sub = data['audio'], data['sub']

# Split data
X_train, X_test, y_train, y_test = train_test_split(audio, sub, test_size=0.2)

# Write to correct format files
with open('training.txt', 'w') as f:
    for audio, sub in zip(X_train, y_train):
        f.write(f'{os.path.join(os.getcwd(), audio)}|{sub}\n')

with open('testing.txt', 'w') as f:
    for audio, sub in zip(X_test, y_test):
        f.write(f'{os.path.join(os.getcwd(), audio)}|{sub}\n')
